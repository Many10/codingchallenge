Coding challenge for Hiberus Technologies.

On this file will explain the way to build/run/use the microservice application.

There's two ways to build/run/use the app:
	1. Using the dockerfile: For this, we need have docker installed. First, we need change the application properties from
							 bill, checkout and logistice service. Open the file application.properties and change this property: eureka.client.service-url.defaultZone and put this new value eureka.client.service-url.defaultZone=http://eureka-server:8761/eureka

							Then, we need to generate the .jar files for every microservice, for this we open the CMD in each microservice root folder and execute the following command: mvnw clean package.

							When every .jar file have been generated, then we locate the CMD in the docker-compose folder, and execute the following command: docker-compose build. This will create each image of the microservices.	

							When this is done, then we execute docker-compose up. This will create and run each image within a docker container. 						

	2. Doing it manually: This is how is configured by default in the repository. We should compile every microservice and 						   execute it in the following order: eureka - bill - logistic - checkout.

So, now we are ready. We can use postman to test the microservices response using this json in the body:

{
    "clientId" : "10904565",
    "date" : "2020-06-27",
    "address" : "Cra 52 #129 sur - 37",
    "products" : [
        {
            "id" : "1",
            "quantity" : "20",
            "cost" : "1000"
        },
        {
            "id" : "2",
            "quantity" : "10",
            "cost" : "10000"
        }
    ]
}

In the browser, also we can search by http://localhost:8761, and this will show us the discovery service created.

Libraries used: 
	- Spring-eureka-client: This library allows create a client to connect with the discovery service.
	- Swagger2: This library creates and provides the api documentation.
	- Swagger-ui: This is a complement to swagger that allows see the api documentation in a html page.
	- Spring-eureka-server: This library allows create a discovery server.
