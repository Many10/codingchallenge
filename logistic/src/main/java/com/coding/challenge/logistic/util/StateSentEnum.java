package com.coding.challenge.logistic.util;

/**
 * Enum class with the sent states names
 * @author Javier Flórez
 *
 */
public enum StateSentEnum {
	
	//Options
	PENDING_STATE("PENDING"),
	PROGRESS_STATE("IN PROGRESS"),
	DISTRIBUTION_STATE("DISTRIBUTION"),
	DELIVERED_STATE("DELIVERED");
	
	private String stateName; //State name
	
	//Enum constructor
	private StateSentEnum(String stateName) {
		this.stateName = stateName;
	}

	public String getStateName() {
		return stateName;
	}	
	
}
