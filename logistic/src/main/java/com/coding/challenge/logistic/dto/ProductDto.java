package com.coding.challenge.logistic.dto;

/**
 * Dto class for object Product
 * @author Javier Flórez
 *
 */
public class ProductDto {

	private int id; //Product identifier
	private int quantity; //Quantity of products to buy
	private double cost; //Product's cost
	
	//Getters and setters
	public int getId() {
		return id;
	}
	public int getQuantity() {
		return quantity;
	}
	public double getCost() {
		return cost;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	
}
