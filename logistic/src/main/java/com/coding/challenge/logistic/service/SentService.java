package com.coding.challenge.logistic.service;

import com.coding.challenge.logistic.dto.OrderDto;
import com.coding.challenge.logistic.dto.SentDto;

/**
 * Interface that defines the methods for SentService class
 * @author Javier Flórez
 *
 */
public interface SentService {

	/**
	 * CreateCheckout method creates the sent to the specified order
	 * @param OrderDto object that contains the order information
	 * @return The SentDto object with the sent information
	 */
	public SentDto createSentOrder(OrderDto order);
	
}
