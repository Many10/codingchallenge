package com.coding.challenge.logistic.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Util class with methods reusables
 * @author Javier Flórez
 *
 */
public class Util {

	/**
	 * Private constructor to avoid creation object
	 */
	private Util() {
		
	}
	
	/**
	 * Util methods gets a date with additional days
	 * @param additionalDays The cant of days that will be added
	 * @return The new date
	 */
	public static Date createDate(int additionalDays) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		if (additionalDays > 0) {
			calendar.add(Calendar.DAY_OF_YEAR, additionalDays);
		}
		return calendar.getTime();
	}

}
