package com.coding.challenge.logistic.dto;

import java.util.Date;
import java.util.List;

/**
 * Dto class for object Order
 * Defines the object
 * @author Javier Florez
 *
 */
public class OrderDto {

	private int clientId; //Client identifier
	private Date date; //Transaction Date
	private String address; //Client address
	private List<ProductDto> products; //List of products to buy

	//Getters and setters
	public int getClientId() {
		return clientId;
	}

	public Date getDate() {
		return date;
	}

	public String getAddress() {
		return address;
	}

	public List<ProductDto> getProducts() {
		return products;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setProducts(List<ProductDto> products) {
		this.products = products;
	}
}
