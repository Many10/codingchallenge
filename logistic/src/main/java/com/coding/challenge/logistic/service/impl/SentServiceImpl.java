package com.coding.challenge.logistic.service.impl;

import org.springframework.stereotype.Service;

import com.coding.challenge.logistic.dto.OrderDto;
import com.coding.challenge.logistic.dto.SentDto;
import com.coding.challenge.logistic.service.SentService;
import com.coding.challenge.logistic.util.StateSentEnum;
import com.coding.challenge.logistic.util.Util;

/**
 * Service class for Logistic application Implements SentService interface that
 * defines the class's methods
 * 
 * @author Javier Flórez
 *
 */
@Service
public class SentServiceImpl implements SentService {

	@Override
	public SentDto createSentOrder(OrderDto order) {
		SentDto sent = new SentDto();
		try {
			if (order != null) {
				sent.setOrder(order);
				sent.setSentId(1);
				sent.setState(StateSentEnum.PENDING_STATE.getStateName()); //The initial state
				sent.setDeliveryDate(Util.createDate(14)); //The maximum waiting days
				sent.setDeliveryCompany("DHL");
			}
		} catch (Exception e) {
			System.out.println("Exception in: createSentOrder, caused by: " + e.getMessage()); 
		}
		return sent;
	}

}
