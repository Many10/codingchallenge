package com.coding.challenge.logistic.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coding.challenge.logistic.dto.OrderDto;
import com.coding.challenge.logistic.dto.SentDto;
import com.coding.challenge.logistic.service.SentService;

/**
 * Controller class for Logistic Service
 * @author Javier Flórez
 *
 */
@RequestMapping("api/v1/sent")
@RestController
public class SentController {

	private final SentService sentService; //Instance of Service that allows get the methods
	
	//Dependency injection
	@Autowired
	public SentController(SentService sentService) {
		this.sentService = sentService;
	}
	
	/**
	 * PostMapping method to receive the request
	 * @param order object that contains the information of the order
	 * @return SentDto that contains the sent information
	 */
	@PostMapping
	public SentDto createSentOrder(@RequestBody OrderDto order) {
		return sentService.createSentOrder(order);
	}
	
}
