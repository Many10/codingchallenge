package com.coding.challenge.bill.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coding.challenge.bill.dto.BillDto;
import com.coding.challenge.bill.dto.OrderDto;
import com.coding.challenge.bill.service.BillService;

/**
 * Rest controller for BillService
 * @author Javier Flórez
 *
 */
@RequestMapping("api/v1/bill")
@RestController
public class BillController {

	private final BillService billService; //Service that access the bussiness
	
	//Dependency injection of Service object
	@Autowired
	public BillController(BillService billService) {
		this.billService = billService;
	}
	
	/**
	 * PostMapping method that receives the request
	 * @param order object with the order information
	 * @return BillDto object created
	 */
	@PostMapping
	public BillDto createBill(@RequestBody OrderDto order) {
		return billService.createBill(order);
	}
	
}
