package com.coding.challenge.bill.dto;

import java.util.Date;

/**
 * Dto class for object Bill
 * Defines the object
 * @author Javier Flórez
 *
 */
public class BillDto {

	private int id; //Bill identifier
	private OrderDto order; //OrderDto belongs to Bill 
	private Date date; //Transaction date
	private double total; //Total amount to pay
	
	//Getters and setters
	public int getId() {
		return id;
	}
	
	public Date getDate() {
		return date;
	}
	
	public double getTotal() {
		return total;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public void setTotal(double total) {
		this.total = total;
	}
	
	public OrderDto getOrder() {
		return order;
	}
	
	public void setOrder(OrderDto order) {
		this.order = order;
	}
	
	
}
