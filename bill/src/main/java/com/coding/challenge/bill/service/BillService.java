package com.coding.challenge.bill.service;

import com.coding.challenge.bill.dto.BillDto;
import com.coding.challenge.bill.dto.OrderDto;

/**
 * Interface that defines the methods for BillService class
 * @author Javier Flórez
 *
 */
public interface BillService {

	/**
	 * CreateBill method creates the bill object to the specified order
	 * @param OrderDto object that contains the order information
	 * @return The BillDto object with the information
	 */
	public BillDto createBill(OrderDto order);
	
}
