package com.coding.challenge.bill.service.impl;

import org.springframework.stereotype.Service;

import com.coding.challenge.bill.dto.BillDto;
import com.coding.challenge.bill.dto.OrderDto;
import com.coding.challenge.bill.dto.ProductDto;
import com.coding.challenge.bill.service.BillService;

/**
 * Service class for Bill application Implements BillService interface that
 * defines the class's methods
 * 
 * @author Javier Florez
 *
 */
@Service
public class BillServiceImpl implements BillService {

	@Override
	public BillDto createBill(OrderDto order) {
		BillDto bill = new BillDto();
		try {
			if(order != null) {
				bill.setId(1);
				bill.setDate(order.getDate());
				bill.setOrder(order);
				// The total is the sum of every cost*quantity product
				bill.setTotal(order.getProducts().stream().mapToDouble(x -> x.getCost() * x.getQuantity()).sum());
			}
		} catch (Exception e) {
			System.out.println("Exception in: createBill, caused by: " + e.getMessage()); //Log trace
		}
		return bill;
	}

}
