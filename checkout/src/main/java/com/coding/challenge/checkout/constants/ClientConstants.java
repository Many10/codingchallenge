package com.coding.challenge.checkout.constants;

public class ClientConstants {

	public static final String URL_BILL_SERVICE = "http://service-bill/api/v1/bill";
	
	public static final String URL_LOGISTIC_SERVICE = "http://service-logistic/api/v1/sent";
}
