package com.coding.challenge.checkout.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coding.challenge.checkout.dto.CheckoutResumeDto;
import com.coding.challenge.checkout.dto.OrderDto;
import com.coding.challenge.checkout.service.CheckoutService;

/**
 * Controller class for Checkout Service
 * @author Javier Flórez
 *
 */
@RequestMapping("api/v1/checkout")
@RestController
public class CheckoutController {

	private final CheckoutService checkoutService; //Instance of Service that allows get the methods
	
	//Dependency Injection
	@Autowired
	public CheckoutController(CheckoutService checkoutService) {
		this.checkoutService = checkoutService;
	}

	/**
	 * PostMapping method to receive the request
	 * @param order object that contains the information of the order
	 * @return The resume of the checkout operation
	 */
	@PostMapping
	public CheckoutResumeDto executeCheckout(@RequestBody OrderDto order) {
		return checkoutService.createCheckout(order);
	}
	
}
