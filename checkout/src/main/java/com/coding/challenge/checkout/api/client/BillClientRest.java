package com.coding.challenge.checkout.api.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.coding.challenge.checkout.constants.ClientConstants;
import com.coding.challenge.checkout.dto.BillDto;
import com.coding.challenge.checkout.dto.OrderDto;

/**
 * Client class that allows the connection to Bill microservice
 * @author Javier Flórez
 *
 */
@Component
public class BillClientRest {
	
	private final RestTemplate clientRest; //Class to make the connection
	
	//Dependency injection
	@Autowired
	public BillClientRest(RestTemplate clientRest) {
		this.clientRest = clientRest;
	}
	
	/**
	 * Method to execute post request to other services
	 * @param newOrder Object that receives the other service
	 * @return The response from the other service
	 */
	public BillDto executePost(OrderDto newOrder) {
		final String resourceUrl = ClientConstants.URL_BILL_SERVICE;
		final BillDto response = clientRest.postForObject(resourceUrl, newOrder, BillDto.class);
		return response;
	}
	
}
