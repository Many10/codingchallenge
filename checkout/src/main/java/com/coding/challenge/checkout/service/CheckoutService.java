package com.coding.challenge.checkout.service;

import com.coding.challenge.checkout.dto.CheckoutResumeDto;
import com.coding.challenge.checkout.dto.OrderDto;

/**
 * Interface that defines the methods for CheckoutService class
 * @author Javier Flórez
 *
 */
public interface CheckoutService {
	
	/**
	 * CreateCheckout method executes the checkout operation to the specified order
	 * @param OrderDto object that contains the order information
	 * @return The CheckoutResumeDto object with the operation information
	 */
	public CheckoutResumeDto createCheckout(OrderDto order);

}
