package com.coding.challenge.checkout.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coding.challenge.checkout.api.client.BillClientRest;
import com.coding.challenge.checkout.api.client.LogisticClientRest;
import com.coding.challenge.checkout.dto.BillDto;
import com.coding.challenge.checkout.dto.CheckoutResumeDto;
import com.coding.challenge.checkout.dto.OrderDto;
import com.coding.challenge.checkout.dto.SentDto;
import com.coding.challenge.checkout.service.CheckoutService;

/**
 * Service class for Checkout application Implements CheckoutService interface
 * that defines the class's methods
 * 
 * @author Javier Florez
 *
 */
@Service
public class CheckoutServiceImpl implements CheckoutService {

	private final BillClientRest billClientRest; // Object to connect to Bill microservice
	private final LogisticClientRest logisticClientRest; // Object to connect to Logistic microservice

	// Dependency injection
	@Autowired
	public CheckoutServiceImpl(BillClientRest billClientRest, LogisticClientRest logisticClientRest) {
		this.billClientRest = billClientRest;
		this.logisticClientRest = logisticClientRest;
	}

	@Override
	public CheckoutResumeDto createCheckout(OrderDto order) {
		CheckoutResumeDto checkoutResume = new CheckoutResumeDto();
		try {
			if(order != null) {
				BillDto bill = billClientRest.executePost(order); //Calling the bill microservice
				SentDto sent = logisticClientRest.executePost(order); //Calling the logistic microservice
				checkoutResume.setBill(bill);
				checkoutResume.setSent(sent);
			}
		} catch (Exception e) {
			System.out.println("Exception in: createCheckout, caused by: " + e.getMessage()); //Log trace
		}
		return checkoutResume;
	}

}
