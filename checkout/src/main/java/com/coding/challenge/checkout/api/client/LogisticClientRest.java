package com.coding.challenge.checkout.api.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.coding.challenge.checkout.constants.ClientConstants;
import com.coding.challenge.checkout.dto.OrderDto;
import com.coding.challenge.checkout.dto.SentDto;

/**
 * Client class that allows the connection to Logistic microservice
 * 
 * @author Javier Flórez
 *
 */
@Component
public class LogisticClientRest {

	private final RestTemplate clientRest; //Class that allows the connection

	//Dependency injection
	@Autowired
	public LogisticClientRest(RestTemplate clientRest) {
		this.clientRest = clientRest;
	}

	/**
	 * Method to execute post request to other services
	 * @param newOrder Object that receives the other service
	 * @return The response from the other service
	 */
	public SentDto executePost(OrderDto newOrder) {
		final String resourceUrl = ClientConstants.URL_LOGISTIC_SERVICE;
		final SentDto response = clientRest.postForObject(resourceUrl, newOrder, SentDto.class);
		return response;
	}

}
