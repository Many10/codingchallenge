package com.coding.challenge.checkout.dto;

import java.util.Date;

/**
 * Dto class for sent model
 * @author Javier Flórez
 *
 */
public class SentDto {
	
	private int sentId; //Sent identifier
	private OrderDto order; //OrderDto object
	private String state; //Sent's state
	private Date deliveryDate; //Delivery date
	private String deliveryCompany; //Company with the shipping

	//Getters and setters
	public int getSentId() {
		return sentId;
	}

	public OrderDto getOrder() {
		return order;
	}

	public String getState() {
		return state;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public String getDeliveryCompany() {
		return deliveryCompany;
	}

	public void setSentId(int sentId) {
		this.sentId = sentId;
	}

	public void setOrder(OrderDto order) {
		this.order = order;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public void setDeliveryCompany(String deliveryCompany) {
		this.deliveryCompany = deliveryCompany;
	}
}
