package com.coding.challenge.checkout.dto;

/**
 * Dto class for checkout resume
 * @author Javier Flórez
 *
 */
public class CheckoutResumeDto {

	private BillDto bill; //The result of BillService
	private SentDto sent; //The result of LogisticService

	//Getters and setter
	public BillDto getBill() {
		return bill;
	}

	public SentDto getSent() {
		return sent;
	}

	public void setBill(BillDto bill) {
		this.bill = bill;
	}

	public void setSent(SentDto sent) {
		this.sent = sent;
	}

}
